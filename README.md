<p align="center">
    <img width=60% src="https://git.afpy.org/AFPy/potodo/raw/branch/main/media/Potodo.png">
</p>
<p align="center">
    <img src="https://img.shields.io/pypi/v/potodo">
    <img src="https://img.shields.io/badge/python-v3.6+-blue.svg">
    <img src="https://img.shields.io/badge/license-MIT-blue.svg">
    <img src="https://img.shields.io/badge/contributions-welcome-orange.svg">
</p>

## What is it ?

Potodo, a (almost) flawless TODO/progress listing CLI tool for po files.

### Potodo is part of poutils!

[Poutils](https://pypi.org/project/poutils) (`.po` utils) is is a metapackage to easily install usefull Python tools to use with po files
and `potodo` is a part of it! Go check out [Poutils](https://pypi.org/project/poutils) to discover the other useful tools for `po` file related translation!


## Installation

```sh
pip install potodo
```

## Usage example

```
usage: potodo [-h] [-p path] [-e path [path ...]] [-a X] [-b X] [-f] [-u API_URL] [-n] [-c] [-j] [--exclude-fuzzy] [--exclude-reserved]
              [--only-reserved] [--show-reservation-dates] [--no-cache] [-i] [-l] [--version] [-v]

List and prettify the po files left to translate.

options:
  -h, --help            show this help message and exit
  -p path, --path path  execute Potodo in path
  -e path [path ...], --exclude path [path ...]
                        gitignore-style patterns to exclude from search.
  -a X, --above X       list all TODOs above given X% completion
  -b X, --below X       list all TODOs below given X% completion
  -f, --only-fuzzy      print only files marked as fuzzys
  -u API_URL, --api-url API_URL
                        API URL to retrieve reservation tickets (https://api.github.com/repos/ORGANISATION/REPOSITORY/issues?state=open or
                        https://git.afpy.org/api/v1/repos/ORGANISATION/REPOSITORY/issues?state=open&type=issues)
  -n, --no-reserved     don't print info about reserved files
  -c, --counts          render list with the count of remaining entries (translate or review) rather than percentage done
  -j, --json            format output as JSON
  --exclude-fuzzy       select only files without fuzzy entries
  --exclude-reserved    select only files that aren't reserved
  --only-reserved       select only only reserved files
  --show-reservation-dates
                        show issue creation dates
  --no-cache            Disables cache (Cache is disabled when files are modified)
  -i, --interactive     Activates the interactive menu
  -l, --matching-files  Suppress normal output; instead print the name of each matching po file from which output would normally have been
                        printed.
  --version             show program's version number and exit
  -v, --verbose         Increases output verbosity
```

## Development setup

Create a virtual environment
```sh
python3 -m venv venv
```

Activate it
```sh
source venv/bin/activate
```

Install the dev requirements
```sh
pip install -r requirements-dev.txt
```

Install the pre-commit hook
```sh
pre-commit install
```

Install `potodo` in a development version
```
pip install -e .
```

## Release History

* v0.21.2
    * FIX: Don't miss issues (reservations) to files containing multiple dots. Contributed by @eviau.
* v0.21.0
    * A nice new README
* v0.20.0
    * New exclude behavior with gitignore style matching !
* v0.19.2
    * Dropped `cache_args` to simplify cache functionality
* v0.19.1
    * Fixed a bug of division by 0
    * Replaced Travis-ci tests with github actions
* v0.19.0
    * Fixed windows support
* v0.17.3
    * Fixed a math error where the completion %age of a folder was wrong
    * Fixes on the `.potodoignore` file
* v0.17.0
    * Added tests
    * Fixed bug where github would rate limit your IP address
    * Fixed argument errors
    * Added `-l` `--matching-files` Which will print the path of files matching your arguments
* v0.16.0
    * Args passed to potodo are now cached as well ! This allows for a better control of what is cached !
    * The ignore file now works as the .gitignore does. Add a venv/ in your .potodoignore for example :)
* v0.15.0
    * Potodo now supports .potodoignore files ! You can finally ignore the venv you made 🎉
* v0.14.3
    * Added cache versioning to avoid errors when cache changes, for example if files are moved between `potodo` versions.
* v0.14.2
    * Nothing new, just code moved around ! Thanks for sticking around 🎉
* v0.14.1
    * Added `--only-reserved` option to display only reserved filed
    * Added `--reserved-dates` to display when a file was reserved
    * Added cache to cache `pofiles` to speedup the reading process
    * Added logging for verbosity
    * Added interactive option with `--interactive`
    * Added contributors in the readme
* < v0.14.1
    * Base version

## Contributing

1. Fork it (<https://git.afpy.org/repo/fork/100>)
2. Create your feature branch (`git checkout -b feature/fooBar`

`/!\` Don't forget to bump the version in `potodo/__init__.py` when you're pushing your changes to your branch

3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
